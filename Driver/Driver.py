import controllers.SourceDbController   as sdc
import controllers.AuditDbController    as adc
import controllers.IngestionController  as igc
import constants.constant               as const
import utilities.DriverConfig           as dc
import managers.GlueManager             as gm
import utilities.MiscUtilities          as mu
import math
import warnings


class JobManager(object):
    def __init__(self, job_name, data_source, source_type, env):
        """Driver Constructor
        :param data_source: source of data eg. Aurora
        :param source_type: structured or unstructured
        :param env: development environment
        Establishes db connections and starts all the required functions
        """
        if job_name is not None and data_source is not  None and source_type is not  None and env is not  None:
            self.job_name       =   job_name
            self.data_source    =   data_source
            self.source_type    =   source_type
            self.env            =   env
            self.process_id     =   -99999
            self.batch_id       =   -1
            self.stage          =   const.INGEST_LANDING_STAGE
            self.audit          =   adc.AuditDbController(self.job_name, const.INGEST_LANDING_STAGE, const.AUDIT_DB_NAME, self.env, self.source_type, self.data_source)
            self.source         =   sdc.SourceDbController(self.job_name, const.INGEST_LANDING_STAGE, const.SOURCE_DB_NAME, self.env, self.source_type, self.data_source)
            self.glue_manager   =   gm.GlueManager(self.env, self.source_type)
            self.driver_job()

        else:
            print('Driver did not received all arguments')

    def driver_job(self):
        """
        Runs the flow of Driver Program
        :return: True if Driver is successful else False
        """

        self.add_process_status()
        ps_id  = self.add_process_statistics(job_type=const.JOB_TYPE_DRIVER, stage=const.INGEST_LANDING_STAGE,
                                             created_by=self.job_name, updated_by=self.job_name)
        self.table_marker_entry()
        self.invoke_ingestion_controller()
        self.mark_driver_completed(ps_id)


    def add_process_status(self):
        """
        Allow only one process to run at a time.
        Add Entry to Process_Status Table and mark it IN_PROGRESS

        """
        # Check if any process status is IN_PROGRESS to make sure only one process runs at a time.
        process_state = self.audit.get_process_state()  # Returns number of process in_progress

        if process_state == 0:
            # No Process is in In_Progress State, you may start a new process
            self.process_id = self.audit.start_process().lastrowid
            print('[INFO][Driver]: Process_Status Entry')

        else:   # Initiate new process flow
            print('[ERROR][Driver]: Process is already In-Progress State')
            raise Exception("A process is already running. Wait until it gets completed")
    def add_process_statistics(self, job_type, stage, source_table='NONE', target_table='NONE', job_count=-1, created_by='NONE', updated_by='NONE'):
        """
        Add Entry to Process Statistics Table.
        :return:
        """
        job_id      = self.glue_manager.get_job_id(self.job_name)
        cursor    = self.audit.add_entry_process_statistics(
                                self.source_type, self.data_source, job_id, job_type, self.process_id,
                                stage, source_table, target_table, self.batch_id, job_count, created_by, updated_by)

        print("[INFO][Driver]: Marked it's entry in Process_Statistics Table")
        return cursor.lastrowid
    def table_marker_entry(self):
        """
        Read Config file. Mark table entries in Table_Marker in AuditDb.
        :return: None
        """
        config_json = dc.DriverConfig().config_file()
        print('[INFO][Driver]: Reading from Driver Config file')
        databases = config_json[self.data_source]['databases']
        for database in databases:      # Iterate on databases
            for table_name in config_json[self.data_source][database]:       # Iterate on tables of each database
                if not self.audit.table_exists(table_name):
                    # If table does not exists then it's first time we are reading table works like full load
                    self.full_load(table_name, config_json[self.data_source][database][table_name])

                else:
                    # Table already exist now need to look for weather it's full load or incremental
                    if config_json[self.data_source][database][table_name]['table_load_type'] == const.INCREMENTAL_LOAD:
                        self.incremental_load(table_name, config_json[self.data_source][database][table_name])
                    else:
                        self.full_load(table_name, config_json[self.data_source][database][table_name])
    def invoke_ingestion_controller(self):
        job_name = const.CONTROLLER_JOB
        process_stat_id = self.add_process_statistics(job_type=const.JOB_TYPE_INGEST_CONTROLLER, stage=const.INGEST_LANDING_STAGE, created_by=job_name, updated_by=job_name)
        print('[INFO][Driver]: Ingestion Controller Invoked')
        igc.ingestion_controller(self.process_id, job_name, self.data_source, self.source_type, self.env, const.JOB_TYPE_INGEST_CONTROLLER)\
            .start_controller(process_stat_id)

        # Create glue job
        # job_id = self.glue_manager.create_job(job_name, self.stage, const.CONTROLLER_JOB, 'bucket_path')
        # Start the Job
        # self.glue_manager.start_job(job_id)
    def mark_driver_completed(self, ps_id):
        """
        Marks the driver job completed in Process_Statistics Table
        :param: ps_id = Process Statistics ID to updated the record
        :return:
        """
        self.audit.update_job_status_in_process_statistics(ps_id, const.COMPLETED, const.JOB_TYPE_DRIVER)
        print('[INFO][Driver]: Driver marked itself Completed')

    # =======    Following are helper methods       ==========
    def is_table_exist(self, table_name):
        """
        Check this table exists in table_marker or not
        :param table_name:
        :return: True, False
        """
        if self.audit.table_exists(table_name):
            return True
        return False
    def full_load(self, table_name, table_info):
        # (process_id, source_type, data_source, batch_id, source_table, source_column, column_start, column_end, job_count, data_size, status, created_at, updated_at, created_by, updated_by, deleted)

        chunk_size              =   int(table_info['batch_size'])
        source_column           =   table_info['column_name']
        column_start            =   table_info['start_value']
        column_end              =   self.source.get_full_load_records(table_name, source_column)
        batch_id                =   mu.generate_batch_id()

        total_records           =   self.source.get_count(table_name)
        job_count               =   -1
        if total_records < chunk_size:
            job_count           =   1
        else:
            job_count           =   math.ceil(total_records / chunk_size)

        self.audit.table_marker_entry(self.process_id, self.source_type, self.data_source, batch_id, table_name,
                                       source_column, column_start, column_end,
                                      job_count, chunk_size, const.IN_PROGRESS, self.job_name, self.job_name, 0)

        print('[INFO][Driver]: Table_Marker Entry Successful for FULL-LOAD')
    def incremental_load(self, table_name, table_info):
        chunk_size              =   int(table_info['batch_size'])
        source_column           =   table_info['column_name']
        batch_id                =   mu.generate_batch_id()

        column_start            =   self.audit.get_last_record(table_name)
        column_end              =   self.source.get_max_column(table_name, source_column)

        # Now Check Records btw the start and end values in source db table.

        delta                   =   self.source.get_delta_records(table_name, source_column, column_start, column_end)
        job_count               =   -1
        if delta == 0:
            # No new record is added
            warnings.warn(message='[WARN][Driver]: No new record is detected for INCREMENTAL')
            return
        elif delta<=chunk_size:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           job_count = 1
        else:
            job_count = math.ceil(delta/chunk_size)


        self.audit.table_marker_entry(self.process_id, self.source_type, self.data_source, batch_id, table_name,
                                      source_column, column_start+1, column_end,
                                      job_count, chunk_size, const.IN_PROGRESS, self.job_name, self.job_name, 0)

        print('[INFO][Driver]: Table_Marker Entry Successful for INCREMENTAL')

def main():
    JobManager(const.JOB_NAME, const.DATA_SOURCE, const.SOURCE_TYPE, const.ENVIRONMENT)

if __name__=='__main__':
    main()