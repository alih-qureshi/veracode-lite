#!/usr/bin/env bash
#   Credentials
aws ssm put-parameter --overwrite --name "/dev/vdl/structured/aurora/source_db_credentials" --type "String" --value {"host":"database-1.cqbmvulvwixt.us-east-1.rds.amazonaws.com","user":"rimsha","password":"rimsha123"}
aws ssm put-parameter --overwrite --name "/dev/vdl/structured/aurora/audit_db_credentials" --type "String" --value {"host":"database-1.cqbmvulvwixt.us-east-1.rds.amazonaws.com","user":"rimsha","password":"rimsha123"}
#   Buckets
aws ssm put-parameter --overwrite --name "/dev/vdl/structured/aurora/source_bucket" --type "String" --value ali-veracode-source
aws ssm put-parameter --overwrite --name "/dev/vdl/structured/aurora/ingestion_landing/ingest_landing_bucket" --type "String" --value ali-ingest-landing
aws ssm put-parameter --overwrite --name "/dev/vdl/structured/aurora/parquet_bucket" --type "String" --value ali-parquet
aws ssm put-parameter --overwrite --name "/dev/vdl/structured/aurora/curated_bucket" --type "String" --value ali-curated

#   Bucket Creation
aws s3api create-bucket --bucket ali-veracode-source --region us-east-1
aws s3api create-bucket --bucket ali-ingest-landing --region us-east-1
aws s3api create-bucket --bucket ali-parquet --region us-east-1
aws s3api create-bucket --bucket ali-curated --region us-east-1

#   Remove Buckets forcefully
aws s3 rb s3://ali-ingest-landing --force
aws s3 rb s3://ali-parquet --force
aws s3 rb s3://ali-curated --force






aws ssm put-parameter --overwrite --name "/dev/vdl/aurora/bucket_type/bucket-name" --type "String" --value "ali-veracode-prep"

aws ssm put-parameter --overwrite --name "/dev/vdl/aurora/ingest-landing/path" --type "String" --value "/structured"

aws ssm put-parameter --overwrite --name "/dev/vdl/aurora/source/aurora" --type "String" --value '{"host":"veracodedl-auditing.cluster-ro-cqqu6doeozf2.us-east-1.rds.amazonaws.com","port":3306,"username":"veracodedl","password":""}'

aws ssm put-parameter --overwrite --name "/dev/vdl/acl-role-arn" --type "String" --value "" # IAM Role

aws ssm put-parameter --overwrite --name "/dev/vdl/source/bucket-name" --type "String" --value "ali-veracode-prep" # Bucket Name where Source Code is placed

aws ssm put-parameter --overwrite --name "/dev/vdl/source/aurora/ingest-landing/worker/config-file-path" --type "String" --value "src/config/structure_ingestion_worker.json"


