
REGION                      = 'us-east-1'

# /{env}/{project_code}/{data_source}/{source_type}/{stage}/{bucket_type}


# JOB_NAME             =   '{JOB_NAME}'
# DATA_SOURCE          =   '{DATA_SOURCE}'
# SOURCE_TYPE          =   '{SOURCE_TYPE}'
# ENVIRONMENT          =   '{ENV}'
# PROJECT_CODE         =   '{PROJECT_CODE}'

JOB_NAME                    =   'Driver'

DATA_SOURCE                 =   'aurora'
SOURCE_TYPE                 =   'structured'
ENVIRONMENT                 =   'dev'
PROJECT_CODE                =   'vdl'

FILE_PATH                   =   'D:/output/csv/'

# JOB NAMES
DRIVER_JOB                  =  'DRIVER'
CONTROLLER_JOB              =  'CONTROLLER'
WORKER_JOB                  =  'WORKER'


JOB_TYPE_DRIVER             = 'DRIVER_JOB'
JOB_TYPE_INGEST_CONTROLLER  = 'INGEST_CONTROLLER_JOB'
JOB_TYPE_INGESTION_WORKER   = 'INGEST_WORKER_JOB'


INCREMENTAL_LOAD            = 'incremental'
FULL_LOAD                   = 'full-load'

# STATES

IN_PROGRESS                 =    'IN_PROGRESS'
COMPLETED                   =    'COMPLETED'
ERROR                       =    'ERROR'
FAILED                      =    'FAILED'

# STAGES
INGEST_LANDING_STAGE        = 'ingestion_landing'
INGEST_PARQUET_STAGE        = 'ingestion_parquet'
RAW_CATALOGUE_STAGE         = 'raw_catalogue'
CURATION_STAGE              = 'curation'
CURATION_CATALOGUE_STAGE    = 'curation_catalogue'

# BUCKETS Types
SOURCE_BUCKET               = 'source_bucket'
INGEST_LANDING_BUCKET       = 'ingest_landing_bucket'
PARQUET_BUCKET              = 'parquet_bucket'
CURATED_BUCKET              = 'curated_bucket'
BUCKET_PREFIX               = '/{env}/{project_code}/{source_type}/{data_source}/{stage}/'


# DATABASES

AUDIT_DB_NAME               = 'ali_audit_db'
SOURCE_DB_NAME              = 'employees'


CONFIG_FILE_LOCATION_KEY    = 'src/configs/driver_config.json'       # Config file will be in SSM



# SSM Keys
SOURCE_DB_CREDENTIALS       = '/{env}/{project_code}/{source_type}/{data_source}/source_db_credentials'
AUDIT_DB_CREDENTIALS        = '/{env}/{project_code}/{source_type}/{data_source}/audit_db_credentials'


# WORKERS

NUM_OF_WORKERS              =       5