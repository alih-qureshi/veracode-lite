import boto3
class GlueManager(object):
    def __init__(self, environment, source_type):
        try:
            self.client = boto3.client('glue')
            self.env    = environment
            self.source_type = source_type
        except Exception as e:
            raise Exception (e)
            print(e)

    def get_job_id(self, job_name):
        return 1
    def start_job(self, job_id):
        pass
    def create_job(self, job_name, stage, executor_name, bucket_name,args=None):
        """
        :param job_name: Job Name
        :param stage: Current Stage of Process
        :param executor_name: Who created the job
        :param bucket_name:
        :param args:
        :return: Job_ID
        """
        self.client.create_job()

    def delete_job(self):
        pass
    def get_all_jobs(self):
        pass


def main():
    pass

if __name__=='main':
    main()