'''

This will handle all database related stuff

'''

import json
import pymysql
from utilities import SsmUtilities as ssm
from constants import constant   as const

class DatabaseManager(object):
    # __connection__ = None
    #
    # @staticmethod
    # def getInstance():
    #     """ Static access method. """
    #     if database_manager.__connection__==None:
    #         database_manager()
    #     return database_manager.__connection__

    def __init__(self, env, stage, source_type='structured', data_source='aurora', dbname=None):
        self.env = env
        self.stage = stage
        self.source_type = source_type
        self.data_source = data_source
        self.dbname = dbname
        self.connection = self.create_connection()


    def create_connection(self):

        try:
            cred = json.loads(ssm.SsmManager().get_ssm_param(const.SOURCE_DB_CREDENTIALS, self.env, self.source_type, self.data_source, self.stage))
            connection = pymysql.connect(host=cred['host'],
                                         user=cred['user'],
                                         password=cred['password'],
                                         db=self.dbname,
                                         charset='utf8mb4',
                                         cursorclass=pymysql.cursors.DictCursor)
            return connection
        except Exception as e:
            print(e)
            return e

    def get_records(self, query):
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query)
                self.connection.commit()
                return cursor.fetchall()
        except Exception as e:
            print(e)
            return e


    def add_records(self, query):
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query)
                self.connection.commit()
                return cursor
        except Exception as e:
            print(e)
            return e


    def print_records(self, cursor):
        for row in cursor:
            print(row)
        print()
        return

def main():

    obj=DatabaseManager('dev', 'ingestion', source_type = 'structured',data_source = "aurora", dbname=const.AUDIT_DB_NAME)
    cur=obj.get_records("SELECT count(*) from file_status limit 10")
    print(cur)
    # obj.print_records(cur)
if __name__ == "__main__":
    main()