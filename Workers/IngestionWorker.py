from controllers import SourceDbController  as sdc
from controllers import AuditDbController   as adc
from constants   import constant            as const
from utilities   import MiscUtilities       as misc
from multiprocessing.pool import ThreadPool
from utilities   import SsmUtilities        as ssm
import datetime
class IngestionWorker:
    def __init__(self, env, process_id, process_statistics_id, table_name, table_info, batch_id, source_type, data_source, job_name, stage):
        self.env                    =       env
        self.process_id             =       process_id
        self.process_statistics_id  =       process_statistics_id
        self.table_name             =       table_name
        self.table_info             =       table_info
        self.batch_id               =       batch_id
        self.source_type            =       source_type
        self.job_name               =       job_name
        self.stage                  =       stage
        self.data_source            =       data_source
        self.threads_list           =       []
        self.source_db              =       sdc.SourceDbController(self.job_name, self.stage, const.SOURCE_DB_NAME, const.ENVIRONMENT, self.source_type, self.data_source)
        self.audit_db               =       adc.AuditDbController(self.job_name, self.stage, const.AUDIT_DB_NAME, const.ENVIRONMENT, self.source_type, self.data_source)
        self.ssm                    =       ssm.SsmManager()
    # 1. Get Data from Table_marker on basis of pId and table_name
    # 2. JobCount = Num of Threads
    # 3.

    # def save_to_csv_thread(self):
    #     # s3://ali-vcdl/structure/aurora/employees/department/2019/departments.parquet-1
    #     # 2019-10-25 16:06:03
    #     jobs = int(self.table_info['jobs_count'])
    #     chunk_size = int(self.table_info['data_size'])
    #     date = datetime.datetime.date(self.table_info['updated_at'])
    #     table_name = self.table_info['source_table']
    #
    #     # file_path = f's3://{const.INGEST_LANDING_BUCKET}/{const.SOURCE_TYPE}/{const.DATA_SOURCE}/{const.SOURCE_DB_NAME}/{table_name}/{date.year}/{date.month}/{date.day}/'
    #     file_path = 'D:/output/csv/'
    #     print(file_path)
    #     for i in range(jobs):
    #         # Make thread
    #         # print(i*chunk_size)
    #         self.source_db.out_file_query_execution(table_name=table_name,offset=i*chunk_size, batch_size=chunk_size, file_path=file_path+'{}-{}.csv'.format(table_name, i))

    def create_thread_pool(self):
        """
        Create multiple threads each thread process different chunk of same table
        :return: None
        """
        # self.audit.add_entry_process_statistics(self.source_type, self.data_source,
        #                                         misc.generate_batch_id(), self.job_type,
        #                                         self.process_id,
        #                                         const.INGEST_LANDING_STAGE,
        #                                         self.table_info['source_table'],
        #                                         self.table_info['source_table'],
        #                                         self.table_info['batch_id'],
        #                                         self.table_info['jobs_count'],
        #                                         const.JOB_TYPE_INGESTION_WORKER,
        #                                         const.JOB_TYPE_INGESTION_WORKER)
        try:
            no_of_threads = int(self.table_info['jobs_count'])
            chunk_size = int(self.table_info['data_size'])
            date = datetime.datetime.date(self.table_info['updated_at'])
            table_name = self.table_info['source_table']

            bucket_name = self.ssm.get_ssm_param(const.BUCKET_PREFIX, self.env, self.source_type, self.data_source, self.stage, const.INGEST_LANDING_BUCKET) #ali_ingest_landing

            # file_path = f's3://{const.INGEST_LANDING_BUCKET}/{const.SOURCE_TYPE}/{const.DATA_SOURCE}/{const.SOURCE_DB_NAME}/{table_name}/{date.year}/{date.month}/{date.day}/'
            file_path = 's3://{}/{}/{}/{}/{}/{}/{}/{}/'.format(bucket_name, self.source_type, self.data_source, const.SOURCE_DB_NAME, table_name, date.year, date.month, date.day)

            # file_path = const.FILE_PATH

            thread_pool = ThreadPool(processes=no_of_threads)  # Define the thread pool to keep track of the sub processes

            for i in range(no_of_threads):
                self.create_thread(thread_pool, i*chunk_size, chunk_size, table_name, file_path)

            thread_pool.close()  # After all threads started we close the pool
            thread_pool.join()  # And wait until all threads are done

            print('[INFO][ING-WORKER]: Worker Completed Successfully]')
            print('[INFO][ING-WORKER]: Marking Worker Completed in ProcessStatistics Table]')

            self.audit_db.update_job_status_in_process_statistics(self.process_statistics_id, const.COMPLETED, const.JOB_TYPE_INGESTION_WORKER)

        except Exception as e:
            print (e)
            print('[ERROR][ING-WORKER]: Worker failed due to some issues]')
            return False


    def create_thread(self, thread_pool, offset, chunk_size, table_name, file_path):
        """
        create a single thread
        :param offset: use to fetch the records from table
        :return: Null
        """
        if thread_pool is None or offset is None:
            print("[ERROR][ING-WORKER]: Thread_pool OR Offset does not exist")
            return
        try:
            file_path = file_path + '{}-{}.csv'.format(table_name, offset)
            t1 = thread_pool.apply_async(self.source_db.out_file_query_execution, (table_name, offset, chunk_size, file_path))
            self.audit_db.add_file_path_in_file_status(self.source_type, self.data_source, self.process_id,
                                                       file_path, table_name, const.INGEST_LANDING_STAGE,
                                                       self.table_info['batch_id'],
                                                       misc.generate_batch_id(), const.IN_PROGRESS,
                                                       const.JOB_TYPE_INGESTION_WORKER, const.JOB_TYPE_INGESTION_WORKER)

            self.threads_list.append(t1)

        except Exception as e:
            print('[ERROR][ING-WORKER]: Error in Threading. '+ str(e))

def main():
    pass

if __name__=='__main__':
    main()
