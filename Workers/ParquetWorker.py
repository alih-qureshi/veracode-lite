from pyspark.sql import SparkSession, SQLContext
# import constants.constant as const
spark = SparkSession.builder.appName('Spark-SQL').enableHiveSupport().getOrCreate()
sc = spark.sparkContext

file_path = 'D:/output/csv/'
df = spark.read\
          .format('csv')\
          .load(file_path+'employees/*')

df.coalesce(1) \
    .write \
    .option('header', 'true') \
    .mode('overwrite')\
    .parquet(file_path+'employees/parquet/employees.parquet')
print('---------------------------------------------')
print('000000000000000000000000000000000000000000',df.count())
print('---------------------------------------------')