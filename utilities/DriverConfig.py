import json
from utilities import  S3Utilities      as s3
from utilities import  SsmUtilities     as ssm
from constants import  constant         as const
class DriverConfig(object):
    def __init__(self):
        self.env                =   const.ENVIRONMENT
        self.data_source        =   const.DATA_SOURCE
        self.project_code       =   const.PROJECT_CODE
        self.bucket_name        =   const.SOURCE_BUCKET
        self.config_file_key    =   const.CONFIG_FILE_LOCATION_KEY

    def config_file(self):
        """
        Reads Config file and return in json format
        :return: json
        """
        with open('../configs/driver_config.json') as file:
            config_json = json.load(file)
            return config_json

        # Get Path of Driver_Config file from SSM

        # config_json = s3.S3Utilities().get_config_file(self.bucket_name, self.config_file_key)
        # return config_json




#
# def main():
#     config =DriverConfig()
#     print(config.config_file())
# if __name__=='__main__':
#     main()