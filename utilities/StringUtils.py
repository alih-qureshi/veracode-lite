from constants import constant as const

class StringUtils:
    def __init__(self):
        pass
    def parse_key(self, key, env, source_type, stage, data_source):
        """
        Replace all the place holders and return SSM key which will be queried later
        :param key - Stubbed key to be replaced
        :param env - environment
        :param source_type - structured or un-structured
        :param bucket_type - type of bucket
        :param stage - stage of the process. ingest_landing etc
        """

        parameter_name = key.replace('{env}', env)\
            .replace('{project_code}', const.PROJECT_CODE)\
            .replace('{source_type}', source_type)\
            .replace('{stage}', stage)\
            .replace('{data_source}', data_source)

        return parameter_name

    def create_job_name(self, process_id, stage, job_type, executor):
        return f"ALI_{executor}_{process_id}_{stage}_{job_type}"
def main():
    conf = StringUtils()
    print(conf.parse_key('/{env}/{project_code}/{data_source}/{source_type}/{stage}'
                         , 'dev', 'structured', 'ingest_landing', 'aurora'))

if __name__ == "__main__":
    main()