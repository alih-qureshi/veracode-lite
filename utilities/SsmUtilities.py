import boto3
import json
import constants.constant as const
from utilities import StringUtils as str_utils
# from constants import constant
class SsmManager(object):
    def __init__(self):
        self.ssm_client = boto3.client('ssm')


    # CONST.DATABASE_CONFIG_PARAMTER, self.environment, self.source_type, self.stage, self.data_source)
    def get_ssm_param_with_key(self, key):
        try:
            response = self.ssm_client.get_parameter(
                Name=key,
                WithDecryption=True
            )
            return response['Parameter']['Value']
        except Exception as e:
            print(e)
            return -1


    def get_ssm_param(self, key, env, source_type, data_source, stage='', bucket_name=None):
        try:
            if bucket_name is None:
                key = str_utils.StringUtils().parse_key(key, env, source_type, stage, data_source)
            else:
                key = str_utils.StringUtils().parse_key(key, env, source_type, stage, data_source) + bucket_name
            # print(key)
            response = self.ssm_client.get_parameter(
                Name=key,
                WithDecryption=True
            )
            return response['Parameter']['Value']
        except Exception as e:
            print(e)
            return -1


def main():
    ssm = SsmManager()

    res = ssm.get_ssm_param(const.AUDIT_DB_CREDENTIALS ,'dev', 'structured', 'aurora', 'db_credentials')
    # res = ssm.get_ssm_param(const.BUCKET_PREFIX ,'dev', 'structured', 'aurora', const.INGEST_LANDING_STAGE, const.INGEST_LANDING_BUCKET)
    print (json.loads(res))
if __name__ == "__main__":
    main()