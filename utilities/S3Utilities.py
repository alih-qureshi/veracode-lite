import boto3
from utilities import SsmUtilities as ssm
from utilities import StringUtils   as const_utils
import json

class S3Utilities(object):
    def __init__(self):
        self.s3_client  = boto3.resource('s3')

    def get_config_file(self, bucket, key):
        """

        :return: Config File in Json Format
        """
        obj = self.s3_client.Object(bucket, key)
        config_file = json.loads(obj.get()['Body'].read().decode('utf-8'))
        return config_file

    def get_object(self, path, env, source_type, bucket_type, stage):
        """
        Gets the object from s3 file path.
        :param env - project environment
        :param source_type - structured or un-structured
        :param bucket_type - source or ingestion or raw or curation
        :param stage - stage of the process
        :return bucket_name
        """

        key = const_utils.ConstUntils().parse_key(path, env, source_type, stage, bucket_type)

        value = ssm.SsmManager().get_ssm_param(key)
        try:
           response = self.s3_client.get_object(
            Bucket=bucket_name,
            Key=key
            )

           s3_object=response["Body"].read().decode()

        except s3_client.exceptions.ParameterNotFound as ex:
            print (ex)


        return s3_object




def main():
    util = S3Utilities()
    print(util.get_config_file('ali-veracode-prep', 'src/configs/driver_config.json'))

if __name__=='__main__':
    main()