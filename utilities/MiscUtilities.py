import uuid


def generate_batch_id():
    return str(uuid.uuid4().fields[-1])[:9]


print(generate_batch_id())