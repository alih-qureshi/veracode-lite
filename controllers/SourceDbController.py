from managers  import DatabaseManager as dbm
from constants import constant        as const

class SourceDbController(object):
    def __init__(self , job_name, stage, db_name='veracode_source', env='dev', source_type='structured', data_source='aurora'):
        """
        :param job_name - the name it uses for created_by field for any inserts and updated_by
        field for any updates it makes
        :param stage: ingestion, curation, catalog
        :param source_type: structured, unstructured
        :param data_source: aurora, ac logs, scan logs, flaw logs
        :param env: the environment in which we are running this audit controller. Default is dev, the value could be qa, uat, production too.
        """
        self.job_name = job_name
        self.stage = stage
        self.db_name = db_name
        self.env = env
        self.source_type = source_type
        self.data_source = data_source
        self.database = dbm.DatabaseManager(self.env, self.stage, self.source_type, self.data_source, self.db_name)

    def get_count(self, table_name):
        sql = "select count(*) as count from {}.{}".format(self.db_name, table_name)
        return int(self.database.get_records(sql)[0]['count'])

    def get_full_load_records(self, table_name, source_column='*'):
        sql = "SELECT {} FROM {}.{} ORDER BY {} DESC LIMIT 1".format(source_column, self.db_name, table_name, source_column)
        return self.database.get_records(sql)[0][source_column]

    def get_max_column(self, table_name, source_column='*'):
        sql = "SELECT max({}) as _max_ FROM {}.{};".format(source_column, self.db_name, table_name)
        return self.database.get_records(sql)[0]['_max_']

    def get_delta_records(self, table_name, source_column, column_start, column_end):
        sql='SELECT COUNT(*) AS delta_records FROM {}.{} WHERE {} > {} AND {} <= {};'\
            .format(self.db_name, table_name, source_column, column_start, source_column, column_end)
        return int(self.database.get_records(sql)[0]['delta_records'])

    def out_file_query_execution(self, table_name, offset, batch_size, file_path):
        """
        :param table_name: Table to be converted into csv
        :param offset: Starting point of each batch file
        :param batch_size: Batch Size
        :param file_path: Data to be stored at location
        :return:
        """
        out_file = f"""
                SELECT * 
                FROM {self.db_name}.{table_name}
                LIMIT {offset}, {batch_size}
                INTO OUTFILE '{file_path}'
                FIELDS TERMINATED BY ',' 
                ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n';
        """
        print(file_path)
        try:
            database = dbm.DatabaseManager(self.env, self.stage, self.source_type, self.data_source, self.db_name)
            database.add_records(out_file)
            database.connection.close()
        except Exception as e:
            print('[ERROR][SourceDB]: Outfile Error')
            return e

def main():
    audit = SourceDbController('ingestion-job', stage='ingestion', db_name=const.SOURCE_DB_NAME, env='dev', source_type='structured', data_source='aurora')
    # print(audit.get_count('employees'))
    # print(audit.get_max_column('employees', 'emp_no'))
    print(audit.get_delta_records('employees', 'emp_no', '10001', '10002'))
    # audit.out_file_query_execution('departments', 0, 100,'E:/file.csv')
if __name__ == "__main__":
    main()