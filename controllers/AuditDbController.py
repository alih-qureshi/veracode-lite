from managers  import DatabaseManager as dbm
from constants import constant        as const
class AuditDbController(object):
    def __init__(self , job_name, stage, db_name='veracode', env='dev', source_type='structured', data_source='aurora'):
        """
        :param job_name - the name it uses for created_by field for any inserts and updated_by
        field for any updates it makes
        :param stage: ingestion, curation, catalog
        :param source_type: structured, unstructured
        :param data_source: aurora, ac logs, scan logs, flaw logs
        :param env: the environment in which we are running this audit controller. Default is dev, the value could be qa, uat, production too.
        """
        self.job_name = job_name
        self.stage = stage
        self.db_name = db_name
        self.env = env
        self.source_type = source_type
        self.data_source = data_source
        self.database = dbm.DatabaseManager(self.env, self.stage, self.source_type, self.data_source, self.db_name)

    def get_process_state(self):
        """
        :return: Returns the current status of process
        """
        query = "SELECT count(status) as count FROM {}.process_status WHERE status='IN_PROGRESS' AND created_at=(SELECT MAX(created_at) FROM {}.process_status);".format(self.db_name, self.db_name)

        # print(self.database.get_records(query)[0]['count'])
        return int(self.database.get_records(query)[0]['count'])

    def start_process(self):
        """
        Adds Entry to Audit DB (PROCESS_STATUS Table) that a program is started and returns it's ProcessID. Mark the process in progress.
        :return: ProcessID:
        """

        query = "INSERT INTO {}.process_status" \
                "(source_type, data_source, status, start_time, created_at, updated_at, created_by, updated_by) " \
                "values( '{}', '{}', '{}', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '{}', '{}');"\
                .format(self.db_name, self.source_type, self.data_source, const.IN_PROGRESS, self.job_name, self.job_name)
        return self.database.add_records(query)

    def table_exists(self, table_name):

        """
        Check if table exists in table_marker or not. If not exist then return 0 other wise number of entries
        :return: boolean
        """
        query = "SELECT COUNT(*) as count " \
                "FROM {}.table_marker " \
                "WHERE source_table='{}'"\
                .format(self.db_name, table_name)
        return int(self.database.get_records(query)[0]['count'])

    def table_marker_entry(self, process_id, source_type, data_source, batch_id, source_table, source_column, column_start, column_end, job_count, data_size, status, created_by, updated_by, deleted):
        sql = "INSERT INTO {}.table_marker" \
              "(source_type, data_source, process_id, batch_id, source_table, source_column, column_start, column_end, batch_time, jobs_count, data_size, status, created_at, updated_at, created_by, updated_by, deleted)" \
              " VALUES ('{}','{}',{},'{}','{}','{}','{}','{}',CURRENT_TIMESTAMP,{},'{}','{}',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'{}','{}', {});"\
            .format(self.db_name, source_type, data_source, process_id, batch_id, source_table, source_column, column_start, column_end, job_count, data_size, status,created_by, updated_by, deleted)

        # print(sql)
        self.database.add_records(sql)


    def add_entry_process_statistics(self, source_type, data_source, job_id, job_type, process_id, stage, source_table='', target_table='', batch_id='', job_record_count=-1, created_by='NONE', updated_by='NONE'):
        """

        :rtype: object
        """
        query = "INSERT INTO process_statistics (source_type, data_source, status, job_id, job_type, stage, process_id, start_time, created_at, source_table, target_table, batch_id, job_records_count, created_by, updated_by) " \
                "VALUES('{}', '{}', '{}', '{}', '{}', '{}', {}, NOW(), NOW(), '{}', '{}', '{}', {}, '{}', '{}');"\
            .format(source_type, data_source, const.IN_PROGRESS, job_id, job_type, stage, process_id, source_table, target_table, batch_id, job_record_count, created_by, updated_by)
        return self.database.add_records(query)

    def get_last_record(self, table_name):
        """
        For Incremental Load it returns the last inserted ID from table marker against last recently created record
        :return: Column End ID of SourceDB table
        """
        sub_query = "SELECT MAX(created_at) FROM table_marker WHERE source_table = '{}'".format(table_name)
        query = "SELECT column_end FROM table_marker WHERE created_at = ({}) AND source_table = '{}';".format(sub_query, table_name)
        return int(self.database.get_records(query)[0]['column_end'])

    def get_all_rows_from_table_marker(self, process_id):
        """
        Retrieve all the records from table_marker
        :param process_id:
        :return: batch_id, table_name
        """
        query = "SELECT * FROM table_marker WHERE process_id={}".format(process_id)
        return self.database.get_records(query)

    def get_free_worker_count(self, process_id):
        try:
            query = "SELECT COUNT(*) as count FROM process_statistics WHERE process_id='{}' AND status='{}';".format(process_id, const.COMPLETED)
            return self.database.get_records(query)[0]['count']
        except Exception as e:
            print('[ERROR]:', e)
            raise Exception (e)

    def update_job_status_in_process_statistics(self, process_stat_id, status, update_by,error=None):
        """
        :param status: Status to be updated
        :param update_by: Which stage is updating the status
        :param process_stat_id: Process_Statistics Table Primary key (Optional)
        :param error:
        :return: None
        """
        try:
            query = "UPDATE process_statistics SET status='{}', end_time=CURRENT_TIMESTAMP, updated_at=CURRENT_TIMESTAMP," \
                    " updated_by='{}', error='{}' WHERE id={};".format(status, update_by, error, process_stat_id)
            self.database.add_records(query)
        except Exception as e:
            print('[ERROR] ', e)
            raise Exception (e)
    def completed_worker_status(self, process_id):
        try:
            query = "SELECT COUNT(*) as count FROM process_statistics " \
                    "WHERE process_id={} AND status='{}' AND job_type='{}';".format(process_id, const.COMPLETED, const.JOB_TYPE_INGESTION_WORKER)
            return self.database.get_records(query)[0]['count']
        except Exception as e:
            print('[ERROR] ', e)
            raise Exception (e)

    def add_file_path_in_file_status(self, source_type, data_source, process_id, file_path, source_table, stage, batch_id, job_id, status, created_by, updated_by):
        try:
            query = "INSERT INTO file_status (source_type, data_source, process_id, file_path, source_table, stage, batch_id, job_id, status, start_time, created_at, updated_at, created_by, updated_by) " \
                    "VALUES('{}', '{}', {}, '{}', '{}', '{}', '{}', '{}', '{}', NOW(), NOW(), NOW(), '{}', '{}');"\
                    .format(source_type, data_source,process_id,file_path, source_table, stage, batch_id, job_id, status, created_by, updated_by)
            self.database.add_records(query)
        except Exception as e:
            print('[ERROR] ', e)
            raise Exception (e)

    def file_status_table_status(self, status, update_by, batch_id, process_id):
        try:
            query = "UPDATE file_status SET status='{}', end_time=CURRENT_TIMESTAMP, updated_at=CURRENT_TIMESTAMP, updated_by='{}'" \
                    " WHERE process_id={} AND batch_id='{}';".format(status, update_by, process_id, batch_id)
            self.database.add_records(query)
        except Exception as e:
            print('[ERROR] ', e)
            raise Exception (e)


def main():
    audit = AuditDbController('ingestion-job', 'ingestion', db_name='veracode', env='dev', source_type='structured', data_source='aurora')
    print(audit.get_free_worker_count(110))
    # print(audit.completed_worker_status(61))

    # for i in range(10):
    #     print(audit.get_all_rows_from_table_marker(71)[i]['batch_time'], )

    # audit.start_process()
    # print(audit.get_process_state())
    # print(audit.table_exists('employees'))
    # print(audit.add_entry_process_statistics
    #       ('structured', 'aurora', 1211, const.JOB_NAME, 49, const.INGEST_LANDING_STAGE, 'employees',
    #        'employees', 'b12322', 10000, const.JOB_NAME, const.JOB_NAME))

    # print(audit.get_last_record('employees'))

    # print(len(audit.get_all_rows_from_table_marker(58)))
    # audit.test()
    # print(audit.usama())
if __name__ == "__main__":
    main()