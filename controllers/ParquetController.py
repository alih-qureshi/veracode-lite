from constants      import constant             as const
from controllers    import SourceDbController   as sdc
from controllers    import AuditDbController    as adc
from Workers        import IngestionWorker      as worker
import utilities.MiscUtilities as misc
class ingestion_controller:
    def __init__(self, process_id, job_name, data_source, source_type, env, job_type):
        self.process_id = process_id
        self.job_name = job_name
        self.data_source = data_source
        self.source_type = source_type
        self.env = env
        self.job_type = job_type

        self.audit = adc.AuditDbController(self.job_name, const.INGEST_LANDING_STAGE, const.AUDIT_DB_NAME, self.env,
                                           self.source_type, self.data_source)
        self.source = sdc.SourceDbController(self.job_name, const.INGEST_LANDING_STAGE, const.SOURCE_DB_NAME, self.env,
                                             self.source_type, self.data_source)


    def parquet_controller_job(self):
        pass

