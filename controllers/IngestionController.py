from constants      import constant             as const
from controllers    import SourceDbController   as sdc
from controllers    import AuditDbController    as adc
from Workers        import IngestionWorker      as worker
import utilities.MiscUtilities as misc
class ingestion_controller:
    def __init__(self, process_id, job_name, data_source, source_type, env, job_type):
        self.process_id = process_id
        self.job_name = job_name
        self.data_source = data_source
        self.source_type = source_type
        self.env = env
        self.job_type = job_type

        self.audit = adc.AuditDbController(self.job_name, const.INGEST_LANDING_STAGE, const.AUDIT_DB_NAME, self.env,
                                           self.source_type, self.data_source)
        self.source = sdc.SourceDbController(self.job_name, const.INGEST_LANDING_STAGE, const.SOURCE_DB_NAME, self.env,
                                             self.source_type, self.data_source)

    def parquet_controller_job(self):
        print("[INFO][PAR-CONTROLLER]: Parquet Controller Invoked")
        max_workers_allowed = const.NUM_OF_WORKERS
        tables = self.audit.get_all_rows_from_table_marker(self.process_id)
        tables_count = len(tables)


    def ingestion_controller_job(self, process_statistics_id):
        # Get Workers from SSM
        max_workers_allowed     =   const.NUM_OF_WORKERS
        tables                  =   self.audit.get_all_rows_from_table_marker(self.process_id)
        tables_count            =   len(tables)
        assigned_to_worker      =   set()
        if tables_count<1:
            raise Exception ("No Tables detected in table marker against given 'process_id'")

        if max_workers_allowed >= tables_count:
            workers_created     =   tables_count
        else:
            workers_created     =   max_workers_allowed

        workers_idle            =   workers_created
        tab                     =   0
        while tables_count>0:
            # Initially assign tables to all allowed workers.
            # If tables = 10 and workers = 5 then workers are assigned 5 tables immediately
            while workers_idle >0 and tables_count>0:
                #tab<workers_created and \

                batch_id = tables[tab]['batch_id']

                if batch_id not in assigned_to_worker:
                    job_name = const.WORKER_JOB+"-"+str(tab)
                    pro_stat_id = self.audit.add_entry_process_statistics(self.source_type, self.data_source,
                                                                          misc.generate_batch_id(), self.job_type,
                                                                          self.process_id,
                                                                          const.INGEST_LANDING_STAGE,
                                                                          tables[tab]['source_table'],
                                                                          tables[tab]['source_table'],
                                                                          tables[tab]['batch_id'],
                                                                          tables[tab]['jobs_count'],
                                                                          const.JOB_TYPE_INGEST_CONTROLLER,
                                                                          const.JOB_TYPE_INGEST_CONTROLLER).lastrowid

                    worker.IngestionWorker(self.env, self.process_id, pro_stat_id, tables[tab]['source_table'], tables[tab], batch_id=batch_id,
                                           source_type=self.source_type, data_source=self.data_source,
                                           job_name=job_name, stage=const.INGEST_LANDING_STAGE) \
                        .create_thread_pool()



                    assigned_to_worker.add(batch_id)
                    workers_idle -= 1
                    tables_count -= 1
                    tab          += 1
                    self.parquet_controller_job()
                    # Worker is completed now trigger the corresponding Parquet Controller



            # free_workers = self.audit.completed_worker_status(self.process_id)
            if workers_idle<1 and tables_count>0:
                free_workers = int(self.audit.get_free_worker_count(self.process_id))
                workers_idle += free_workers

        #   Now everything is done mark the Ingestion Controller Completed
        print('[INFO][ING-CONTROLLER]: Marked itself completed.')
        self.audit.update_job_status_in_process_statistics(process_statistics_id, const.COMPLETED, const.JOB_TYPE_INGEST_CONTROLLER)




    def start_controller(self, process_statistics_id):
        if self.job_type == const.JOB_TYPE_INGEST_CONTROLLER:
            self.ingestion_controller_job(process_statistics_id)
        else:
            # Parquet Controller Stuff will be here
            self.parquet_controller_job()


def main():
    controller = ingestion_controller( 61,const.JOB_NAME, const.DATA_SOURCE, const.SOURCE_TYPE, const.ENVIRONMENT, const.JOB_TYPE_INGEST_CONTROLLER)
    controller.ingestion_contoller_job()
    # controller.start_controller()


if __name__=='__main__':
    main()